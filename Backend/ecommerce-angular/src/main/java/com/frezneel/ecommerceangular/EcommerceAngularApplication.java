package com.frezneel.ecommerceangular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceAngularApplication.class, args);
	}

}
